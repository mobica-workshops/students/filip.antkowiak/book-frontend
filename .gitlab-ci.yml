stages:
  - configure
  - test
  - release
  - deploy

###############################################
# Configure stage
###############################################
# Generate secrets
configure:
  stage: configure
  image: registry.gitlab.com/ldath-core/docker/python:3.10-ansible
  before_script:
    - ansible --version
  script:
    - ./gitlab-configure.sh
  artifacts:
    expire_in: 1 week
    paths:
      - secrets/*

# Generate node modules
yarn_install:
  image: node:18
  script:
    - yarn install
  artifacts:
    expire_in: 1 week
    paths:
      - node_modules
  stage: configure

###############################################
# test stage
###############################################
# Run UT tests with coverage enabled
unit_tests:
  stage: test
  image: node:18
  script:
    - yarn coverage
  dependencies:
    - yarn_install
    - configure
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage/cobertura-coverage.xml


###############################################
# Release stage
###############################################

gcp_registry_latest:
  stage: release
  variables:
    DOCKER_FILE: multi-stage.Dockerfile
    ARTIFACT_REGISTRY_CORE: europe-central2-docker.pkg.dev
    ARTIFACT_REGISTRY_PATH: ${ARTIFACT_REGISTRY_CORE}/coe-devops-cloud-sandbox/docker
    CONTAINER_TARGET_IMAGE: $ARTIFACT_REGISTRY_PATH/$CI_PROJECT_PATH:latest
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"https://${ARTIFACT_REGISTRY_CORE}\":{\"username\":\"_json_key_base64\",\"password\":\"${GCP_ARTIFACT_KEY}\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context "${CI_PROJECT_DIR}" --dockerfile "${CI_PROJECT_DIR}/${DOCKER_FILE}" --destination "${CONTAINER_TARGET_IMAGE}"
  dependencies:
    - configure
    - unit_tests
  only:
    - main
  when: manual
###############################################
# Deploy stage
###############################################

staging:
  stage: deploy
  variables:
    PROJ_ENV: staging
  image: alpine/k8s:1.27.4
  before_script:
    - if [ $PROJ_ENV == "production" ]; then export KUBECONFIG=$KUBE_CONFIG_PROD ; else export KUBECONFIG=$KUBE_CONFIG_STAG ; fi
    - helm repo add --username gitlab-ci-token --password $CI_JOB_TOKEN workshops https://gitlab.com/api/v4/projects/39005882/packages/helm/stable
  script:
    # You can use kubectl to check if you have valid
    - kubectl get po --namespace="workshops-${PROJ_ENV}"
    # For DEBUG - only when it is installed in this case
    - helm upgrade -i -f "secrets/${PROJ_ENV}-values.yaml" --set "nameOverride=${CI_PROJECT_NAME}-${CI_PROJECT_ID},fullnameOverride=${CI_PROJECT_NAME}-${CI_PROJECT_ID}" --namespace="workshops-${PROJ_ENV}" "${PROJ_ENV}-${CI_PROJECT_NAME}-${CI_PROJECT_ID}" workshops/book-service --version 0.5.3 --dry-run --debug
    # This one is deploying
    - helm upgrade -i -f "secrets/${PROJ_ENV}-values.yaml" --set "nameOverride=${CI_PROJECT_NAME}-${CI_PROJECT_ID},fullnameOverride=${CI_PROJECT_NAME}-${CI_PROJECT_ID}"  --namespace="workshops-${PROJ_ENV}" "${PROJ_ENV}-${CI_PROJECT_NAME}-${CI_PROJECT_ID}" workshops/book-service --version 0.5.3 --wait
  dependencies:
    - configure
    - gcp_registry_latest
  environment:
    name: $PROJ_ENV
    url: $PROJ_URL_STAG
  only:
    - main
  when: manual

cleanup:
  stage: deploy
  variables:
    PROJ_ENV: staging
  image: alpine/k8s:1.27.4
  before_script:
    - if [ $PROJ_ENV == "production" ]; then export KUBECONFIG=$KUBE_CONFIG_PROD ; else export KUBECONFIG=$KUBE_CONFIG_STAG ; fi
  script:
    - helm delete --namespace="workshops-${PROJ_ENV}" "${PROJ_ENV}-${CI_PROJECT_NAME}-${CI_PROJECT_ID}"
  only:
    - main
  when: manual